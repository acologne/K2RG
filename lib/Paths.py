#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Define classes modelising paths alignements among a cycle from a bcc

import sys
from sortedcontainers import SortedDict
#import pysam

WARN_READMIX=False # Indicate if a warning corresponding to a mixture of read length have allready been printed
## SAM format
ID=0
FLAG=1
CHR=2
START=3
CIGAR=5
SEQ=9
##
## SAM FLAG parameters
Z=17 # For SAM flag binary conversion
U=Z-3 # if 1, segment is unmapped
RC=Z-5 # if 1, read on reverse strand
P=Z-9 # if 1, not primary alignment
S=Z-12 # if 1, supplementary alignment
##

class Bloc:
	# Bloc object : single ungapped alignement
	
	def __init__(self, start=-1, end=-1, ins=[], insLen=[], d=[], delLen=[], seq=""):
		self.seq = seq.upper() # The sequence of the bloc
		self.SS=None # First and last two nucleotides of S (only if the bloc is an intron)
		self.s = start # start of the bloc
		self.e = end # end of the bloc
		self.ins = ins # List of position within s and e of an insertion
		self.insLen = insLen # List of length of the insertions
		self.d = d # List of position within s and e of a deletion
		self.delLen = delLen # List of length of the deletions
		self.annot = {} # Dictionnary of annotations covering the bloc : d[geneName] = {strand="+/-", biotype="", trName[], trID=[], type="CDS/3UTR/5UTR/None"] (MAYBE SUBDIVISE ONTO 2 DICTIONNARIES, FOR FULL AND PARTIAL COVERAGE?)
		#self.isAnnotatedS = False # is the start position an annotated splice site ?
		#self.isAnnotatedE = False # is the end position an annotated splice site ?
		self.haveSNP = False # Does the bloc contain a SNP ? (N character in the aligned sequence)
		self.isVar=False # Does the bloc correspond to an upper-path bloc ?
		self.varStart=-1 # start of the variable part if the bloc contain one
		self.varEnd=-1 # end of the variable part if the bloc contain one
		self.intStart=-1 # start of the intron if the bloc contain one
		self.intEnd=-1 # end of the intron if the bloc contain one

		if "N" == self.seq:
			self.haveSNP = True

	def __eq__(self, other):
		if not isinstance(other, Bloc):
			return False
		return [self.s,self.e,self.ins,self.insLen,self.d,self.delLen,self.varStart,self.varEnd]==[other.s,other.e,other.ins,other.insLen,other.d,other.delLen,other.varStart,other.varEnd]

	def __len__(self):
		return self.e-self.s+1

	def __str__(self):
		r=str(self.s)+"-"+str(self.e)+" (len: "+str(len(self))+")"
		for i in range(len(self.ins)):
			ins=self.ins[i]
			l=self.insLen[i]
			r+=" ; insertion (len : "+str(l)+") at "+str(ins)
		for i in range(len(self.d)):
			d=self.d[i]
			l=self.delLen[i]
			r+=" ; deletion (len : "+str(l)+") at "+str(d)
		if self.SS!=None:
			r+=" ; "+self.SS+" intron"
		if self.isVar:
			r+=" ; Var. part: "+str(self.varStart)+"-"+str(self.varEnd)
		return r

	def inCDS(self, lID=None):
		if lID==None:
			lID=list(self.annot.keys())
		for gID in lID:
			if gID in list(self.annot.keys()):
				for tID in list(self.annot[gID]["transcripts"].keys()):
					d=self.annot[gID]["transcripts"][tID]
					if d["type"] in ["CDS", "5UTR-CDS", "CDS-3UTR"]:
						return True
		return False

	def strSE(self):
		# string start-end
		return str(self.s)+"-"+str(self.e)

	def getInsUniq(self):
		r=[]
		for i in range(len(self.ins)):
			r.append(str(self.ins[i])+"|"+str(self.insLen[i]))
		return r

	def getDelUniq(self):
		r=[]
		for i in range(len(self.d)):
			r.append(str(self.d[i])+"|"+str(self.delLen[i]))
		return r

	def setSS(self,nRmS,nRmE):
		# nRmS/E : number of base to remove from the start/end of the sequence to get the intron only
		self.intStart=nRmS
		self.intEnd=len(self.seq)-nRmE
		intSeq=self.seq[self.intStart:self.intEnd]
		self.SS=intSeq[:2]+"/"+intSeq[-2:]

	def setVarPos(self, lowBloc1=None, lowBloc2=None):
		# lowBlocs1 is the rightmost lower bloc, lowBloc2 the leftmost
		# For alt events, if lowBlocs1 is set, then the start of the variable path will be the end+1 of lowBloc1
		# Else, the end of the variable part will be the start-1 of lowBloc2
		self.isVar=True
		if lowBloc1==None:
			self.varStart=self.s
		else:
			self.varStart=lowBloc1.e+1
		if lowBloc2==None:
			self.varEnd=self.e
		else:
			self.varEnd=lowBloc2.s-1
			

	def annotBloc(self,dAnnot, s,e,strand,startSeen, stopSeen, sPhase, ePhase):
		# Annotate a bloc with a feature
		gID=dAnnot["gene_id"][0]
		tID=dAnnot["transcript_id"][0]
		if not gID in list(self.annot.keys()):
			# Gene (bio)type
			if "gene_biotype" in list(dAnnot.keys()):
				bt=dAnnot["gene_biotype"][0]
			elif "gene_type" in list(dAnnot.keys()):
				bt=dAnnot["gene_type"][0]
			else:
				print("WARN:\tNo gene_(bio)type found for "+gID)
				bt="NA"
			# Gene name
			if "gene_name" in list(dAnnot.keys()):
				gn=dAnnot["gene_name"][0]
			else:
				print("WARN:\tNo gene_name found for "+gID)
				gn="NA"
			self.annot[gID]={	"strand":strand,
						"biotype":bt,
						"name":gn,
						"transcripts":{}
					}
		if not tID in list(self.annot[gID]["transcripts"].keys()):
			if not startSeen:
				t="5UTR"
			else:
				if not stopSeen:
					t="CDS"
				else:
					t="3UTR"
			ccdsid=None
			if "ccdsid" in list(dAnnot.keys()):
				ccdsid=dAnnot["ccdsid"][0]
			protid=None
			if "protein_id" in list(dAnnot.keys()):
				protid=dAnnot["protein_id"][0]
			# Transcript (bio)type
			if "transcript_biotype" in list(dAnnot.keys()):
				tb=dAnnot["transcript_biotype"][0]
			elif "transcript_type" in list(dAnnot.keys()):
				tb=dAnnot["transcript_type"][0]
			else:
				print("WARN:\tNo transcript_(bio)type found for "+tID)
				tb="NA"
			# Transcript name
			if "transcript_name" in list(dAnnot.keys()):
				tn=dAnnot["transcript_name"][0]
			else:
				print("WARN:\tNo transcript_name found for "+tID)
				tn="NA"
			# Exon id
			if "exon_id" in list(dAnnot.keys()):
				eid=dAnnot["exon_id"][0]
			else:
				print("WARN:\tNo exon_id found for "+tID)
				eid="NA"
			# Exon number
			if "exon_number" in list(dAnnot.keys()):
				en=int(dAnnot["exon_number"][0])
			else:
				print("WARN:\tNo exon_number found for "+tID)
				en=-1
			self.annot[gID]["transcripts"][tID]={	"name":tn,
								"biotype":tb,
								"exon_id":eid,
								"exon_n":en,
								"sPhase":str(sPhase),
								"ePhase":str(ePhase),
								"sExon":s,
								"eExon":e,
								"type":t,
								"CCDS":str(ccdsid),
								"protein_id":str(protid)}

	def getAnnotatedSS(self):
		res=[]
		for gID in list(self.annot.keys()):
			for tID in list(self.annot[gID]["transcripts"].keys()):
				d=self.annot[gID]["transcripts"][tID]
				if not d["sExon"] in res:
					res.append(d["sExon"])
				if not d["eExon"] in res:
					res.append(d["eExon"])
		return res

	def strAnnot(self):
		r=""
		if self.annot=={}:
			return r
		for gID in list(self.annot.keys()):
			d1=self.annot[gID]
			r+="gID ("+d1["name"]+", strand "+d1["strand"]+", "+d1["biotype"]+")"
			i=1
			n=str(len(list(d1["transcripts"].keys())))
			for tID in list(d1["transcripts"].keys()):
				d2=d1["transcripts"][tID]
				r+="\n\tTranscript "+str(i)+"/"+n+": "+tID+" ("+d2["name"]+", "+d2["biotype"]+")"
				r+="\n\t\tBloc in: "+d2["type"]
				r+="\n\t\tStarting phase: "+d2["sPhase"]
				r+="\n\t\tEnding phase: "+d2["ePhase"]
				r+="\n\t\tExon start: "+str(d2["sExon"])
				r+="\n\t\tExon end: "+str(d2["eExon"])
				r+="\n\t\tCCDS: "+d2["CCDS"]
				r+="\n\t\tProtein: "+d2["protein_id"]
				i+=1
			r+="\n"
		return r
			
			

class Path:
	# Path object : single alignement
	
	def __init__(self, algLine=None,countOption=2,exonicReads=True,pairedEnd=False,order=None,dBlocs=None):
		self.bcc = None
		self.cycle = None
		self.ul = None # up/low
		self.len = 0 # Path length from KisSplice header
		self.Slen = 0 # S bloc length obtain by comparing an upper and lower path
		self.c = None # chromosome
		self.s = -1 # start
		self.e = -1 # end
		self.strand = "?" # +, - or ? if unknown (should never happen!) -> ALIGNMENT STRAND (may be different from the reference strand!)
		self.strCounts = "" # Line from the header corresponding to counts
		self.counts = {} # Directory corresponding to counts from the header
		self.rawQuantif = {} # Number of raw reads (from self.counts) supporting this path : d{replicat} = rawQuantif. Replicat is either X or X,Y if paired-end
		self.flag = None # SAM flag
		self.blocs = [] # list of blocs
		self.normFact=1 # Normalisation factor (effSize/Slen)

		if algLine!=None: # If we have an alignement line, then...
			self.fill(algLine,dBlocs) # fill the object with basic header informations
			self.setStrand() # find the alignment strand (SAM FLAG)
			self.countReads(countOption,exonicReads,pairedEnd,order) # make raw counts-related dictionnaries (counts and rawQuantif)
			#print(self)

	def getTranscript(self,lID=None):
		if lID==None:
			lID=self.getGene()
		dTid={} # d[tID]=[occurence, exon_number, tuple] -> if exons were not following each others, the occurence is set to 0
		lT=[] # l[(transcriptID,transcriptName,biotype), ...]
		n=0 # number of blocs
		for b in self.blocs:
			n+=1
			for gID in lID:
				if gID in list(b.annot.keys()):
					for tID in list(b.annot[gID]["transcripts"].keys()):
						d=b.annot[gID]["transcripts"][tID]
						if not tID in list(dTid.keys()):
							tup=(gID,tID)
							lT.append(tup)
							dTid[tID]=[1, d["exon_n"], tup]
						elif d["exon_n"]==dTid[tID][1]+1 or d["exon_n"]==dTid[tID][1]-1:
							dTid[tID][0]+=1
							dTid[tID][1]=d["exon_n"]
						else:
							dTid[tID][0]=0
		# If a transcript ID was present in all the blocs, we return only this transcript ID
		res=[]
		for tID in list(dTid.keys()):
			if dTid[tID][0]==n:
				res.append(dTid[tID][2])
		#if res!=[]:
		return res
		# Else, we return all found transcripts as we can not pick one
		#return lT

	def getGene(self):
		dGid={} # d[gID]=occurence
		lG=[] # l[(geneID,geneName,strand,biotype), ...]
		n=0 # number of blocs
		for b in self.blocs:
			n+=1
			for gID in list(b.annot.keys()):
				if not gID in list(dGid.keys()):
					d=b.annot[gID]
					tup=(gID,d["name"],d["strand"],d["biotype"])
					lG.append(tup)
					dGid[gID]=[0, tup]
				dGid[gID][0]+=1
		# If a gene ID was present in all the blocs, we return only this geneID
		res=[]
		for gID in list(dGid.keys()):
			if dGid[gID][0]==n:
				res.append(dGid[gID][1])
		if res!=[]:
			return res
		# Else, we return all found genes as we can not pick one
		return lG

	def setNormFact(self, effSize, Slen):
		self.Slen=Slen
		self.normFact=float(effSize)/(self.Slen+effSize)

	def getNormQuantif(self):
		if self.ul=="l":
			return self.rawQuantif
		normQuantif={}
		for k in self.rawQuantif:
			normQuantif[k]=self.rawQuantif[k]*self.normFact
		return normQuantif

	def getNormQuantifSep(self):
		if self.ul=="l":
			return self.rawQuantifSep
		normQuantif={}
		for k in self.rawQuantifSep:
			normQuantif[k]=self.rawQuantifSep[k]*self.normFact
		return normQuantif

	def __str__(self):
		r=" -- "+self.getBccCycle()
		r+="\n\tPath : "+self.ul
		r+="\n\tStrand : "+self.strand+" ; Length : "+str(self.len)+" ; S bloc length : "+str(self.Slen)
		r+="\n\tAlignement : "+self.getPos()+" ; Flag "+self.flag+" ; Number of blocs : "+str(self.nBlocs())
		r+="\n\tBlocs :\t"+"\n\t\t".join([self.c+":"+e for e in self.getStrBlocList()])
		r+="\n\tSplice sites : "+" ".join([self.c+":"+str(e) for e in self.getSS()])
		r+="\n\tJunctions : "+" ".join([self.c+":"+e for e in self.getJunc()])
		r+="\n\tCounts string : "+self.strCounts
		r+="\n\tCounts dictionnary : "+str(self.counts)
		r+="\n\tRaw quantification dictionnary : "+str(self.rawQuantif)
		if self.ul=="u":
			quantif=self.getNormQuantif()
			r+="\n\tNormalisation factor :\t"+str(self.normFact)
			r+="\n\tQuantification dictionnary : "+str(quantif)
		else:
			r+="\n\t(Normalised counts and quantification dictionnaries are identical to counts and raw quantifications dictionnaries)"
		return r		

	def countReads(self, countOption, exonicReads, pairedEnd, order):
		dC = {} # d[X]=counts, identical to KisSplice with counts = 0. X is the digit located after AS/SB/ASSB/S/AB/C
		if self.ul=="l" or countOption=="0": # if the path is a lower path, or an upper path with count=0, then we have nothing to compute.
			for k in list(self.counts.keys()):
				X = "".join([x for x in k if x.isdigit()])
				dC[X]=self.counts[k]
		else:
			# If the path is a upper path...
			if countOption=="1": # We have AS, SB, ASSB : it is the same as doing counts=2 with exonicReads = False
				exonicReads = False
				n = len(list(self.counts.keys()))/3 # number of sample
			else: # We have AS, SB, ASSB and S
				n = len(list(self.counts.keys()))/4
			# We consider we are on counts = 2. We must use the AS, SB and ASSB counts.
			n=int(n)
			for X in range(1, n+1):
				X=str(X)
				AS="AS"+X
				SB="SB"+X
				ASSB="ASSB"+X
				ASq=self.counts[AS]
				SBq=self.counts[SB]
				ASSBq=self.counts[ASSB]
				dC[X]=(ASq-ASSBq)+(SBq-ASSBq)+ASSBq
				if exonicReads: # We add the S counts
					S="S"+X
					Sq=self.counts[S]
					dC[X]+=Sq
		# We now sum pairedEnd if needed
		if pairedEnd:
			m=len(list(dC.keys()))
			if m/2!=int(m/2): # We must have an even number of sample
				sys.exit('ERROR:\tOdd number of samples for paired-end run!')
			if order == None: # The pairs follow each other.
				order=[str(i)+","+str(i+1) for i in range(1,m+1) if i%2]
			# We go throw the order list and add couples with each-other
			for couple in order:
				lCouple=couple.split(",")
				self.rawQuantif[couple]=0
				for X in lCouple:
					self.rawQuantif[couple]+=dC[X]
		else:
			self.rawQuantif=dC
		self.rawQuantifSep=dC

	def getSS(self):
		# return the list of splice sites in the path
		n=self.nBlocs()
		if n<2:
			return []
		r=[]
		r.append(self.blocs[0].e)
		for i in range(1,n-1):
			b=self.blocs[i]
			r.append(b.s)
			r.append(b.e)
		r.append(self.blocs[n-1].s)
		if len(r)%2: # If there is an odd number of SS
			sys.exit('ERROR:\tOdd number of splice sites for path '+self.getID()+' composed of the following blocs :\n'+str(self.printBlocs()))
		return r

	def getJunc(self):
		# return the list of junctions in the path
		lSS=self.getSS()
		r=[]
		n=len(lSS)
		i=0
		while i<n:
			r.append(str(lSS[i]+1)+"-"+str(lSS[i+1]-1))
			i+=2
		return r
	
	def getPos(self):
		if not self.isMapped():
			return "unmapped"
		return self.c+":"+str(self.s)+"-"+str(self.e)
	
	def getBccCycle(self):
		return "bcc_"+self.bcc+"|Cycle_"+self.cycle

	def getID(self):
		return self.getBccCycle()+"|"+self.ul+"_path"

	def setStrand(self):
		if self.isReverse():
			self.strand="-"
		else:
			self.strand="+"

	def fill(self, algLine, dBlocs):
		global FLAG
		global START
		global CHR
		global CIGAR
		global ID
		global SEQ
		algLine=algLine.strip()
		lLine=algLine.split("\t")
			
		# Header informations
		bcc=0
		cycle=1
		uplow=3
		name=lLine[ID]
		lName=name.split("|")
		self.bcc=lName[bcc].split("_")[1]
		self.cycle=lName[cycle].split("_")[1]
		self.ul=lName[uplow][0]
		self.len=int(lName[uplow].split("length_")[1].split("_")[0])
		if len(lLine)>4: # The path is quantified			
			countStart=4
			countEnd=-1
			lCounts=lName[countStart:countEnd]
			self.strCounts="|".join(lCounts)
			for count in lCounts:
				lCount=count.split("_")
				self.counts[lCount[0]]=int(lCount[1])

		# SAM flag
		self.flag=lLine[FLAG]
		
		if self.isMapped():
			# chromosome
			self.c=lLine[CHR]
			if self.c not in list(dBlocs.keys()):
				dBlocs[self.c]=SortedDict()
		
			# Start
			self.s=int(lLine[START])

			# Blocs
			self.makeBlocs(lLine[CIGAR], lLine[SEQ], dBlocs[self.c])

	def makeBlocs(self, CIGAR, seq, dB):
		# Read a CIGAR string and makes appropriate blocs (uses self.start)
		# Also set self.e (genomic end of path)
		cStart=self.s
		cEnd=cStart
		digit=""
		ins=[]
		insLen=[]
		d=[]
		delLen=[]
		iS=0 # start indice on the sequence to know if the bloc contain a SNP
		iE=0 # end indice on the sequence to know if the bloc contain a SNP
		for e in CIGAR:
			if e.isdigit():
				digit+=e
			else:
				digit=int(digit)
				if e in ["M","=","X"]: # A match does not creat any bloc
					cEnd+=digit-1
					iE+=digit
				elif e == "N": # A gap creat a bloc
					b=Bloc(cStart, cEnd, ins, insLen, d, delLen, seq[iS:iE])
					if b.s in list(dB.keys()):
						if b in dB[b.s]:
							index=dB[b.s].index(b)
						else:
							dB[b.s].append(b)
							index=-1
					else:
						dB[b.s]=[b]
						index=0
					self.blocs.append(dB[b.s][index])
					ins=[]
					insLen=[]
					cStart=cEnd+digit+1
					cEnd=cStart
					iS=iE
				elif e == "D": # A deletion will be added as additional information in one bloc
					d.append(cEnd+1)
					delLen.append(digit)
					cEnd+=digit+1
					iE+=digit
				elif e == "I": # An insertion will be added as additional information in one bloc
					ins.append(cEnd+1)
					insLen.append(digit)
					cEnd+=1
					iE+=digit
				digit=""
		b=Bloc(cStart, cEnd, ins, insLen, d, delLen, seq[iS:iE])
		if b.s in list(dB.keys()):
			if b in dB[b.s]:
				index=dB[b.s].index(b)
			else:
				dB[b.s].append(b)
				index=-1
		else:
			dB[b.s]=[b]
			index=0
		self.blocs.append(dB[b.s][index])
		self.e=cEnd

	def getIns(self):
		# return the list of the insertions in all the blocs of the path
		r=[]
		for b in self.blocs:
			r+=b.getInsUniq()
		return r

	def getDel(self):
		# return the list of the insertions in all the blocs of the path
		r=[]
		for b in self.blocs:
			r+=b.getDelUniq()
		return r

	def getStrBlocList(self):
		r=[]
		for b in self.blocs:
			r.append(str(b))
		return r

	def getStrBlocs(self):
		lB=self.getStrBlocList()
		return " ; ".join(lB)

	def printBlocs(self):
		lB=self.getStrBlocList()
		for b in lB:
			print((self.c+":"+b))

	def __len__(self): # get genomic RANGE (that is not the length of the path!)
		return self.e-self.s+1
	
	def __getitem__(self, n):
		return self.blocs[n]

	def nBlocs(self):
		return len(self.blocs)

	def starts(self):
		r = []
		for b in self.blocs:
			r.append(b.s)
		return r
	
	def ends(self):
		r = []
		for b in self.blocs:
			r.append(b.e)
		return r

	def coords(self):
		r = []
		for b in self.blocs:
			r.append(b.coord())
		return r

	def sizes(self):
		r = []
		for b in self.blocs:
			r.append(len(b))
		return r
	
	def size(self): # return the path length (sum of bloc length)
		lLen=self.sizes()
		r=0
		for i in lLen:
			r+=i
		return r

	def asbin(self):
		global Z
		# converted the SAM flag to its binary rep (padded with 0's)
		return str(bin(int(self.flag)))[2:].zfill(Z)

	def isMapped(self):
		# is the path mapped ?
		global U
		return self.asbin()[U] == "0"

	def isReverse(self):
		# is the path reverse aligned ?
		global RC
		return self.asbin()[RC] == "1"

	def isPrimary(self):
		# is the path a primary alignment ?
		global P
		return self.asbin()[P] == "1"

	def isSupp(self):
		# is the path a suuplementary alignment ?
		global S
		return self.asbin()[S] == "1"

	def delSeq(self):
		for b in self.blocs:
			b.seq=""

	def getBlocsLen(self):
		r=[]
		for b in self.blocs:
			r.append(len(b))
		return r

class Paths:
	# Paths object : Alignments from the same BCC cycle

	def __init__(self, paths=[]):
		self.up = [] # list of Path
		self.low = [] # list of Path
		self.events=[] # list of event object
		self.repState=None # either regular_event, exact_repeat, inexact_repeat, other_repeat, not_mapped, missed_type2, one_path_mapped
		for p in paths:
			self.add(p)

	def __str__(self):
		r="BCC Cycle state: "+str(self.repState)
		r+="\nNumber of upper path(s): "+str(len(self.up))+" ; Number of lower path(s): "+str(len(self.low))+" ; Number of event: "+str(len(self.events))
		i=1
		for e in self.events:
			r+="\nEvent n°"+str(i)+":"
			r+="\n"+str(e)
			i+=1
		return r

	def writeEvents(self, dOut, fGTF):
		nPW=1
		oReg=dOut["regular_event"]
		if self.repState=="regular_event":
			for event in self.events:
				nPW=event.write(oReg, fGTF, self.repState, nPW)
		else:
			# Writing in the corresponding file
			for event in self.events:
				nPW=event.write(dOut[self.repState], fGTF, self.repState, nPW)
			# Writing a line in the regular output file for each non-regular bcc_cycle
			paths=self.up+self.low
			UNK="NA"
			pos="multiple"
			ev="-"
			if self.up!=[]:
				varPartLen=str(self.up[0].Slen)
				fs=str(self.up[0].Slen%3==0)
				countsUp=self.up[0].strCounts
			else:
				varPartLen=UNK
				fs=UNK
				countsUp=UNK
			if self.low!=[]:
				countsLow=self.low[0].strCounts
			else:
				countsLow=UNK
			cds=False
			i=0
			while i<len(self.up) and not cds:
				p=self.up[i]
				for b in p.blocs:
					if b.isVar and b.inCDS():
						cds=True
				i+=1
			cds=str(cds)
			knownSS=UNK
			bSizeUp=UNK
			bSizeLow=UNK
			if self.repState not in ["not_mapped","one_path_mapped","missed_type2"]:
				rE=self.events[0]
				if rE.upP!=None:
					countsUp=rE.upP.strCounts
					bSizeUp=",".join([str(x) for x in rE.getBlocsSizeUp()])
					if bSizeUp=="":
						bSizeUp="-"
				if rE.lowP!=None:
					countsLow=rE.lowP.strCounts
					bSizeLow=",".join([str(x) for x in rE.getBlocsSizeLow()])
					if bSizeLow=="":
						bSizeLow="-"
			SSup=UNK
			SSlow=UNK
			para=self.repState
			comp="-"
			haveSNP=UNK
			bccCycle=paths[0].getBccCycle()
			PSI=""
			if self.events!=[]:
				PSI=",".join(self.events[0].psis)
			if PSI=="":
				PSI=UNK
			da="-"
			lG=[]
			for event in self.events:
				for tup in event.getGenes():
					if tup not in lG:
						lG.append(tup)
			if lG==[]:
				gID=UNK
				gN=UNK
				strand=UNK
				bio=UNK
				cds=UNK
			else:
				lID=[]
				lName=[]
				lStr=[]
				lBio=[]
				for tup in lG:
					lID.append(tup[0])
					lName.append(tup[1])
					lStr.append(tup[2])
					lBio.append(tup[3])
				gID=",".join(lID)
				gN=",".join(lName)
				strand=",".join(lStr)
				bio=",".join(lBio)
			lTW=[gID,gN,pos,strand,ev,varPartLen,fs,cds,bio,knownSS,bSizeUp,SSup,para,comp,haveSNP,bccCycle,bSizeLow,SSlow,PSI,countsUp,countsLow,da]
			oReg.write("\n"+"\t".join(lTW))
			
			

	def allPrimaryUp(self):
		return allPrimary(self.up)

	def allPrimaryLow(self):
		return allPrimary(self.low)

	def allPrimary(self, lPath):
		# Return True of all the paths from a list of paths are primary alignments
		for p in lPath:
			if not p.isPrimary():
				return False
		return True

	def setRepState(self):
		# Rules to get the bcc state (as compared to the events states):
		## BCC_STATE 		: EVENTS_STATE rule
		## regular_event 	: exactly 1 regular
		## exact_repeats	: m regular (m>1)
		## inexact_repeats	: n inexact (n>0)
		## other_repeats	: n exact + n inexact (n>0)
		## missed_type2		: n type 2 + x regular/exact/inexact (n>0, x>=0)
		## not_mapped		: exactly 1 not_mapped
		## one_path_mapped	: n one_path_aligned (n>0)
		## throw error		: m not mapped (m>1)
		lEventState=[x.state for x in self.events] # list of events states
		slEventState=list(set(lEventState)) # list of uniq events states
		if "type2" in lEventState: # type2 is one of the events state
			self.repState="missed_type2"
			return
		if len(slEventState) == 1: # only one type of event state is present
			if "inexact" in lEventState:
				self.repState="inexact_repeats"
				return
			if "one_path_mapped" in lEventState:
				self.repState="one_path_mapped"
				return
			if "not_mapped" in lEventState:
				self.repState="not_mapped"
				return
			if len(lEventState) == 1: # only one event
				if "regular" in lEventState:
					self.repState="regular_event"
					return
			else: # multiple events of the same state
				if "regular" in slEventState:
					self.repState="exact_repeats"
					return
				sys.exit("ERROR:\tUnable to set the BCC_Cycle state from this events state list: "+str(lEventState))
		elif len(slEventState) == 2 and "inexact" in slEventState and "regular" in slEventState:
			self.repState="other_repeats"
			return
		sys.exit("ERROR:\tUnable to set the BCC_Cycle state from this events state list: "+str(lEventState))

	def getEventsMappedPairs(self):
		r=[]
		for event in self.events:
			r.append(event.mappedPairs)
		return r

	def isAlonePath(self):
		# Return True if the Paths object is composed only by upper or lower paths
		return self.up==[] or self.low==[]

	def compSlen(self):
		# Compute the size of the S bloc
		if self.isAlonePath(): # If we only have one path on the two, we can not compute the size of S. We set it to 0.
			return 0
		return self.up[0].len-self.low[0].len

	def makeEvents(self, threshold):
		# For each pairs of up+low path, make an event
		# Also define the rep state of the bcc
		if self.up==[]:
			for lowP in self.low:
				self.events.append(Event(None, lowP, threshold))
		elif self.low==[]:
			for upP in self.up:
				self.events.append(Event(upP, None, threshold))
		else:
			lExactUp=[]
			lExactLow=[]
			for upP in self.up:
				for lowP in self.low:
					event=Event(upP, lowP, threshold)
					self.events.append(event)
					if event.mappedPairs:
						# The paths must be paired to be exact
						if upP not in lExactUp:
							lExactUp.append(upP)
						if lowP not in lExactLow:
							lExactLow.append(lowP)
			# If a pair of event have an exact match, remove all other unpaired events containing exact upper and lower path
			i=0
			while i < len(self.events):
				event=self.events[i]
				if not event.mappedPairs and event.upP in lExactUp and event.lowP in lExactLow:
					del self.events[i]
				else:
					i+=1
		self.setRepState() # We define the repetition state of the event
			

	def add(self, p): # add a Path object
		if p.ul=="u":
			self.up.append(p)
		else:
			p.delSeq() # We won't need the sequence information of the lower path
			self.low.append(p)
		
	def getStarts(self, m="all"):
		if m=="all":
			return list(set(self.getStarts("up")+self.getStarts("low")))
		elif m=="up":
			l = self.up
		elif m=="low":
			l = self.low
		else:
			return []
		r = []
		for p in l:
			r.append(p.chr+":"+str(p.s))
		return list(set(r))

	def getEnds(self, m="all"):
		if m=="all":
			return list(set(self.getEnds("up")+self.getEnds("low")))
		elif m=="up":
			l = self.up
		elif m=="low":
			l = self.low
		else:
			return []
		r = []
		for p in l:
			r.append(p.chr+":"+str(p.e))
		return list(set(r))

	def getCoords(self, m="all"):
		if m=="all":
			return list(set(self.getCoords("up")+self.getCoords("low")))
		elif m=="up":
			l = self.up
		elif m=="low":
			l = self.low
		else:
			return []
		r = []
		for p in l:
			r.append(p.chr+":"+str(p.s)+str(p.e))
		return list(set(r))

	def getChrom(self, m="all"):
		if m=="all":
			return list(set(self.getChrom("up")+self.getChrom("low")))
		elif m=="up":
			l = self.up
		elif m=="low":
			l = self.low
		else:
			return []
		r = []
		for p in l:
			r.append(p.chr)
		return list(set(r))

	def getBlocs(self):
		r=[]
		for upP in self.up:
			r+=upP.blocs
		for lowP in self.low:
			r+=lowP.blocs
		return list(set(r))

class Event: # one event (pair of paths)

	def __init__(self, upP, lowP, threshold=0):
		self.upP=upP 		# upper path (can be None)
		self.lowP=lowP		# lower path (can be None)
		#self.geneName=[]	# Name of genes overlapping the path
		#self.geneID=[]		# ID -------------------------------
		#self.strand=[]		# Strands --------------------------
		#self.CDS=[]
		#self.biotype=None
		self.eUpBlocs=[]	# event up blocs where unnecesary blocs are removed
		self.eLowBlocs=[]	# event low blocs where unnecesary blocs are removed
		self.eventType=None	# Type of the event (ES, ES_MULTI, ES_altA, ES_altD, ES_altAD, ES_MULTI_altXY, altA, altD, altAD, IR, insertion, deletion, SNP, -)
		self.complex = []	# List of (bcc,cycle,eventID) overlapping this event
		self.complexRegion="NA"	# Genomic localisation of the region where events overlap
		self.psis = []
		self.paired = True	# Are both paths mapped ?
		self.mappedPairs = True	# Are both paths mapped in the same place ?
		self.state = "regular"	# State of the event : type2 (lower path have more than 1 uniq junction), one_path_mapped, not_mapped, inexact (paths aligned at different locations), regular

		# Chromosome_and_genomic_position : held by upP/lowP
		# Variable_part_length / Frameshift_? : held by upP.Slen
		# Gene_names / gene_IDs / strand / CDS_? / Gene_biotype : held by blocs of the lower path. If there is no annotation, strand is defined by getPathStrand()
		# number_of_known_splice_sites/number_of_SNPs : held by blocs of the lower and upper path
		# genomic_blocs_size_(upper_path) / genomic_position_of_each_splice_site_(upper_path)/of_each_SNP : held by blocs of upP
		# paralogs_? : held by Paths.repState
		# snp_in_variable_region : held by upP.blocs.haveSNP
		# Event_name : held by up or low path bcc-cycle
		# genomic_blocs_size_(lower_path) / genomic_position_of_each_splice_site_(lower_path)/of_each_SNP : held by blocs of upP
		# Read_coverage(upper_path) : held by upP.strCounts
		# Read_coverage(lower_path) : held by lowP.strCounts
		# Canonical_sites? : should be held by a path, but it is yet not supported. We need a fasta (and .fai) file of the reference to do that! We can only do that with IR.

		iMU=self.isMappedUp()
		iML=self.isMappedLow()
		
		if not iMU or not iML: # One or both of the paths of the bcc_cycle was not mapped
			if not iMU and not iML:
				self.state="not_mapped"
			else:
				self.state="one_path_mapped"
				if iML:
					self.eLowBlocs=self.lowP.blocs
				else:
					self.eUpBlocs=self.upP.blocs
			self.paired=False
			self.mappedPairs=False
			self.eventType="-"
		elif self.upP.getPos()!=self.lowP.getPos(): # The paths did not mapped at the same place (inexact)
			self.state="inexact"
			self.mappedPairs=False
			self.eventType="-"
			self.eUpBlocs=self.upP.blocs
			self.eLowBlocs=self.lowP.blocs
		else: # regular event
			self.setEventBlocs() # this will remove blocs from junctions that are common to both paths and return a list of bloc list for the upper and lower path
			self.setEventType(threshold) # based on number of blocs and their splice sites positions
		self.setPsi()

	def write(self,o, fGTF, repState, nPW):
		UNK="NA"
		# BCC_cycle ID
		bccCycle=self.getBccCycle()
		# Gene-related informations
		lG=self.getGenes() # [(geneID, geneName, strand, biotype), ...]
		lID=[]
		if lG==[]:
			gID=UNK
			gN=UNK
			strand=self.getPathsStrand()
			lStr=[strand]
			bio=UNK
			cds=UNK
			#knownSS="0"
			lAnnotatedSSup=[]
			lAnnotatedSSlow=[]
			lNewSSup=self.getSSup()
			lNewSSlow=self.getSSlow()
		else:
			lName=[]
			lStr=[]
			lBio=[]
			for tup in lG:
				lID.append(tup[0])
				lName.append(tup[1])
				lStr.append(tup[2])
				lBio.append(tup[3])
			gID=",".join(lID)
			gN=",".join(lName)
			strand=",".join(lStr)
			bio=",".join(lBio)
			cds=str(self.inCDS(lID))
			lAnnotatedSSup,lNewSSup=self.getSSupByAnnot()
			lAnnotatedSSlow,lNewSSlow=self.getSSlowByAnnot()
		lAnnotSS=list(set(lAnnotatedSSup+lAnnotatedSSlow))
		lNewSS=list(set(lNewSSup+lNewSSlow))
		nAnnotSS=len(lAnnotSS)
		nNewSS=len(lNewSSup+lNewSSlow)
		nSS=nAnnotSS+nNewSS
		if nAnnotSS==0:
			knownSS="no_known_ss"
		elif nNewSS==0:
			knownSS="all_splice_sites_known_("+str(nSS)+"_ss)"
		else:
			knownSS=str(nAnnotSS)+"_ss_annotated_(over_"+str(nSS)+"_ss):"+",".join([str(x) for x in sorted(lAnnotSS)])
		lSSup=sorted(lAnnotatedSSup+lNewSSup)
		lSSlow=sorted(lAnnotatedSSlow+lNewSSlow)
		if self.eventType in ["insertion","indel"]:
			lSSup+=self.getInserUp()
			lSSlow+=self.getInserLow()
		if self.eventType in ["deletion","indel"]:
			lSSup+=self.getDelUp()
			lSSlow+=self.getDelLow()
		if lSSup==[]:
			SSup="-"
		else:
			SSup=",".join([str(x) for x in lSSup])
		if lSSlow==[]:
			SSlow="-"
		else:
			SSlow=",".join([str(x) for x in lSSlow])
		# Position / commplexity
		pos=UNK
		comp="-"
		if self.state=="regular":
			pos=self.upP.getPos()
			comp=str(len(self.complex))
		elif self.state=="inexact":
			pos=self.upP.getPos()+","+self.lowP.getPos()
		elif self.upP!=None:
			pos=self.upP.getPos()
		elif self.lowP!=None:
			pos=self.lowP.getPos()
		# Event type
		ev=self.eventType
		ev4=ev[-4:]
		lEv=[]
		refStr=self.getPathsStrand()
		for s in lStr:
			if s==refStr or ev4 not in ["altA","altD"]:
				lEv.append(ev)
			else:
				if ev4=="altA":
					add="altD"
				else:
					add="altA"
				lEv.append(ev[:-4]+add)
		if len(set(lEv))==1:
			ev=lEv[0]
		else:
			ev=",".join(lEv)
		# Pralogues
		para=self.state
		# Var part len / frameshift
		if self.upP!=None:
			varPartLen=str(self.upP.Slen)
			fs=str(self.upP.Slen%3==0)
		else:
			varPartLen=UNK
			fs=UNK
		# SNP
		haveSNP=str(self.haveSNP())
		# PSI
		PSI=",".join(self.psis)
		if PSI=="":
			PSI=UNK
		# Read coverage /  Blocs size
		if self.upP!=None:
			countsUp=self.upP.strCounts
			bSizeUp=",".join([str(x) for x in self.getBlocsSizeUp()])
			if bSizeUp=="":
				bSizeUp="-"
		else:
			countsUp=UNK
			bSizeUp=UNK
		if self.lowP!=None:
			countsLow=self.lowP.strCounts
			bSizeLow=",".join([str(x) for x in self.getBlocsSizeLow()])
			if bSizeLow=="":
				bSizeLow="-"
		else:
			countsLow=UNK
			bSizeLow=UNK
		# Don/acc intron
		da=self.getDonAccIR(list(set(lStr)))
		# TO WRITE
		lTW=[gID,gN,pos,strand,ev,varPartLen,fs,cds,bio,knownSS,bSizeUp,SSup,para,comp,haveSNP,bccCycle,bSizeLow,SSlow,PSI,countsUp,countsLow,da]
		o.write("\n"+"\t".join(lTW))
		# GTF if needed
		if fGTF!=None:
			# Format :
			# chr	<const/var>	exon	<start>	<end>	. 	<strand>	. 	<attributes>
			# attributes={gene_id, gene_name, gene_source, transcript_name, transcript_id, counts, psis, transcripts_through_name, transcript_through_id, event_in, frame, transcript_through_biotype, real_feature, rep_state, event_state}
			## gene_source = "k2rg"
			## transcript_name = "bcc_X|Cycle_Y"
			## transcript_id = "X_Y_i"
			## transcripts_through = transcript going through the PATH
			## real_feature = "bloc"
			lTu, lTl=self.getTranscripts(lID)
			if self.upP!=None and self.isMappedUp():
				self.writeGTF(fGTF, nPW, self.upP, strand, gID, gN, bio, bccCycle, lTu, countsUp, PSI, repState, para, cds, ev)
				nPW+=1
			if self.lowP!=None and self.isMappedLow():
				self.writeGTF(fGTF, nPW, self.lowP, strand, gID, gN, bio, bccCycle, lTl, countsLow, PSI, repState, para, cds, ev)
				nPW+=1
		return nPW
			
#17	const	exon	79207232	79207272	.	-	.	gene_id "unknown"; gene_name "ENTHD2"; gene_source "k2rg"; transcript_name "ENTHD2_1"; transcript_id "ENTHD2_1"; path_names "bcc_139162|Cycle_0|Type_1"; counts "NA"; adj_pval "NA"; psis "NA"; dPSI "NA"; warnings "NA"; transcripts_through_name ""; transcripts_through_id ""; event_in ""; frame ""; transcripts_biotype ""; real_feature "bloc";

	def writeGTF(self, fGTF, nPW, p, strand, gID, gN, bio, bccCycle, lT, counts, PSI, repState, repEvent, cds, ev):
		cdsb=[]
		phases=[]
		if lT==[]:
			geneID=gID
			geneName=gN
			gBiotype=bio
			transcriptID="NA"
			transcriptName="NA"
			transcriptBiotype="NA"
			cdsb="NA"
			phases="NA"
		else:
			lGid=[]
			lGn=[]
			lGbio=[]
			lStr=[]
			lTid=[]
			lTname=[]
			lTbio=[]
			for tup in lT:
				b=p.blocs[0]
				gID=tup[0]
				tID=tup[1]
				lGid.append(gID)
				lGn.append(b.annot[gID]["name"])
				lGbio.append(b.annot[gID]["biotype"])
				lStr.append(b.annot[gID]["strand"])
				d=b.annot[gID]["transcripts"][tID]
				lTid.append(tID)
				lTname.append(d["name"])
				lTbio.append(d["biotype"])
			geneID=",".join(set(lGid))
			geneName=",".join(set(lGn))
			gBiotype=",".join(set(lGbio))
			strand=",".join(set(lStr))
			transcriptID=",".join(lTid)
			transcriptName=",".join(lTname)
			transcriptBiotype=",".join(lTbio)
		if cds=="True":
			cds="CDS"
		else:
			cds="UTR"
		quantif=p.getNormQuantif()
		lq=[]
		for k in quantif:
			lq.append(k.replace(",","-")+":"+str(quantif[k]))
		sq=",".join(lq)
		dAtt={"gene_id":geneID, "gene_name":geneName, "gene_biotype":gBiotype, "gene_source":"k2rg", "transcript_id":bccCycle+"_"+p.ul, "transcript_through_name":transcriptName, "transcript_through_id":transcriptID, "transcript_through_biotype": transcriptBiotype, "counts":counts, "psis":PSI, "rep_state":repState, "rep_event":repEvent, "cds_path":cds, "real_feature":"bloc", "transcript_name":bccCycle+"_"+str(nPW), "event":ev, "quantif":sq}
		for b in p.blocs:
			if cdsb!="NA":
				cdsb=[]
				phases=[]
				for tup in lT:
					gID=tup[0]
					tID=tup[1]
					d=b.annot[gID]["transcripts"][tID]
					cdsb.append(d["type"])
					phases.append(d["sPhase"]+"|"+d["ePhase"])
				cdsb=",".join(cdsb)
				phases=",".join(phases)
			dAtt["cds_bloc"]=cdsb
			dAtt["transcript_through_phases"]=phases
			sAtt=""
			for k in dAtt:
				sAtt+=k+" \""+dAtt[k]+"\"; "
			sAtt=sAtt.strip()
			cv="const"
			if b.isVar:
				cv="var"
			lTW=[p.c, cv, "exon", str(b.s), str(b.e), ".", strand, ".", sAtt]
			fGTF.write("\t".join(lTW)+"\n")

	def getInserUp(self):
		return self.getInser(self.eUpBlocs)

	def getInserLow(self):
		return self.getInser(self.eLowBlocs)

	def getInser(self, lBlocs):
		r=[]
		for b in lBlocs:
			r+=b.ins
		return r

	def getDelUp(self):
		return self.getDel(self.eUpBlocs)

	def getDelLow(self):
		return self.getDel(self.eLowBlocs)

	def getDel(self, lBlocs):
		r=[]
		for b in lBlocs:
			r+=b.d
		return r

	def getBccCycle(self):
		if self.upP!=None:
			return self.upP.getBccCycle()
		return self.lowP.getBccCycle()

	def getBlocsSizeUp(self):
		return self.getBlocsSize(self.eUpBlocs)

	def getBlocsSizeLow(self):
		return self.getBlocsSize(self.eLowBlocs)

	def getBlocsSize(self, lBlocs):
		r=[]
		for b in lBlocs:
			r.append(len(b))
		return r

	def getDonAccIR(self, lStr):
		n=len(lStr)
		if self.eventType=="IR":
			if "+" in lStr:
				ss1=self.eUpBlocs[0].SS
				if n==1:
					return ss1
			if "-" in lStr:
				dComp={"A":"T", "T":"A", "C":"G", "G":"C", "N":"N", "/":"/"}
				ss2=""
				for e in self.eUpBlocs[0].SS[::-1]:
					ss2+=dComp[e]
				if n==1:
					return ss2
			return ss1+"(+),"+ss2+"(-)"
		else:
			return "-"

	def haveSNP(self):
		for b in self.eUpBlocs:
			if b.isVar and b.haveSNP:
				return True
		return False

	def inCDS(self, lID):
		# Return True if the variable part is in the CDS of a gene in lID
		for b in self.eUpBlocs:
			if b.isVar and b.inCDS(lID):
				return True
		return False

	def getTranscripts(self, lID=None):
		lTup=[]
		lTlow=[]
		if self.upP!=None:
			lTup=self.upP.getTranscript(lID)
		if self.lowP!=None:
			lTlow=self.lowP.getTranscript(lID)
		return (lTup,lTlow)

	def getGenes(self):
		lGup=[]
		lGlow=[]
		if self.upP!=None:
			lGup=self.upP.getGene()
		if self.lowP!=None:
			lGlow=self.lowP.getGene()
		# If the upper and lower path shares a common gene, we return it
		res=[]
		for tup in lGlow:
			if tup in lGup:
				res.append(tup)
		if res!=[]:
			return res
		# Else, we return all the possible genes as we can not pick one
		return list(set(lGup+lGlow))
	
	def getSSupByAnnot(self):
		return self.getSSbyAnnot(self.eUpBlocs)

	def getSSlowByAnnot(self):
		return self.getSSbyAnnot(self.eLowBlocs)

	def getSSbyAnnot(self, lBlocs):
		lAnnotSS=[]
		lNewSS=[]
		n=len(lBlocs)
		if n<2:
			return [[],[]]
		i=0
		for b in lBlocs:
			lASS=b.getAnnotatedSS()
			if i!=0:
				if b.s in lASS:
					lAnnotSS.append(b.s)
				else:
					lNewSS.append(b.s)
			if i!=(n-1):
				if b.e in lASS:
					lAnnotSS.append(b.e)
				else:
					lNewSS.append(b.e)
			i+=1
		return [lAnnotSS,lNewSS]

	def getSSup(self):
		return self.getSS(self.eUpBlocs)

	def getSSlow(self):
		return self.getSS(self.eLowBlocs)

	def getSS(self, lBlocs):
		# return the list of splice sites in the path
		n=len(lBlocs)
		if n<2:
			return []
		r=[]
		r.append(lBlocs[0].e)
		for i in range(1,n-1):
			b=lBlocs[i]
			r.append(b.s)
			r.append(b.e)
		r.append(lBlocs[n-1].s)
		if len(r)%2: # If there is an odd number of SS
			sys.exit('ERROR:\tOdd number of splice sites for path '+self.getID()+' composed of the following blocs :\n'+str(self.printBlocs()))
		return r			

	def isMappedUp(self):
		return self.isMapped(self.upP)

	def isMappedLow(self):
		return self.isMapped(self.lowP)

	def isMapped(self, path):
		if path==None or not path.isMapped():
			return False
		return True

	def isSamePaths(self):
		# Indicate if the upP and lowP are the same in terms of genomic position
		if self.upP==None or self.lowP==None:
			return False
		if self.upP.c!=self.lowP.c or self.upP.nBlocs()!=self.lowP.nBlocs():
			return False
		for i in range(self.upP.nBlocs()):
			if self.upP[i].strSE()!=self.lowP[i].strSE():
				return False
		return True

	def setEventType(self, threshold):
		if self.state!="regular": # Type2, one path aligned, not mapped or inexact state
			self.eventType="-"
			return
		if self.isSamePaths(): # The paths are composed of the same blocs (same start / end position)
			# This is either a SNP, an insertion, a deletion or an indel (both insertion and deletion).
			sameIns = self.upP.getIns() == self.lowP.getIns()
			sameDel = self.upP.getDel() == self.lowP.getDel()
			if sameIns and sameDel: # Same insertions : this is a SNP
				self.eventType="SNP"
			else: # Different insertions/deletion
				if not sameIns:
					if not sameDel:
						self.eventType="indel"
					else:
						self.eventType="insertion"
				else:
					self.eventType="deletion"
			return
		# From this point, the lower path is always of length 2. The event type is determined by examinating the number of upper blocs (nUp)
		nUp=len(self.eUpBlocs)
		strand=self.getPathsStrand() # Strand definition for a non annotated event (will change altA -> altD etc...)
		if nUp==1: # Upper path is composed by 1 bloc : IR or del
			if len(self.eUpBlocs[0])>=threshold:
				self.eventType="IR"
				self.eUpBlocs[0].setVarPos(self.eLowBlocs[0],self.eLowBlocs[1])
				self.eUpBlocs[0].setSS(len(self.eLowBlocs[0]),len(self.eLowBlocs[1]))
			else:
				self.eventType="del"
			#self.upP.delSeq() # We won't need the seq information anymore (used only for IR)
			return
		self.upP.delSeq() # We won't need the seq information anymore (used only for IR)
		if nUp==2: # Upper path is composed by 2 blocs : alt event
			self.eventType="alt"
			A=""
			D=""
			if self.eUpBlocs[0].e!=self.eLowBlocs[0].e:
				self.eUpBlocs[0].setVarPos(self.eLowBlocs[0])
				if strand=="-":
					A="A"
				else:
					D="D"
			if self.eUpBlocs[1].s!=self.eLowBlocs[1].s:
				self.eUpBlocs[1].setVarPos(None, self.eLowBlocs[1])
				if strand=="-":
					D="D"
				else:
					A="A"
			self.eventType+=A+D
		else: # Upper path is composed by 3 blocs or more : ES event
			l=["ES"]
			alt=""
			A=""
			D=""
			if nUp>3:
				l.append("MULTI")
			if self.eUpBlocs[0].e!=self.eLowBlocs[0].e:
				self.eUpBlocs[0].setVarPos(self.eLowBlocs[0])
				alt="alt"
				if strand=="-":
					A="A"
				else:
					D="D"
			if self.eUpBlocs[-1].s!=self.eLowBlocs[1].s:
				self.eUpBlocs[-1].setVarPos(None, self.eLowBlocs[1])
				alt="alt"
				if strand=="-":
					D="D"
				else:
					A="A"
			if alt!="":
				l.append(alt+A+D)
			self.eventType="_".join(l)
			for o in range(1,(nUp-1)):
				self.eUpBlocs[o].setVarPos()

	def getPathsStrand(self):
		if self.upP==None:
			return self.lowP.strand
		if self.lowP==None:
			return self.upP.strand
		if self.upP.strand==self.lowP.strand: # Strand definition for a non annotated event (will change altA -> altD etc...)
			return self.upP.strand 
		# If the paths aligned on different strand (I don't know of it's possible), we will use the forward strand
		return "+"
			
		
	def setEventBlocs(self):
		self.eLowBlocs=[]
		self.eUpBlocs=[]
		# this will remove blocs from junctions that are common to both paths and return a list of bloc list for the upper and lower path
		upBlocs=self.upP.blocs
		lowBlocs=self.lowP.blocs
		if len(lowBlocs)<=2: # The lower path must be composed at most of 2 blocs. If this is already the case, we have nothing to do!
			self.eUpBlocs=upBlocs
			self.eLowBlocs=lowBlocs
			return
		# We have to remove some blocs common to both paths
		## Searching the last common bloc before the divergent bloc and the first common bloc after the divergent bloc
		i=1 # lowBlocs index
		j=1 # upBlocs index
		startCom=None # Blocs before this one should be removed (both upper and lower path)
		endCom=None # Blocs after this one should be removed (upper path)
		# We get the junctions of the upper and lower path and we remove the common junctions from the junction of the lower path.
		jLowUniq=[x for x in self.lowP.getJunc() if x not in self.upP.getJunc()] # This must have a length of one
		if len(jLowUniq)!=1:
			self.state = "type2"
			#print("WARNING:\tLower path have more than one uniq junction for bcc_"+self.upP.bcc+"|Cycle_"+self.upP.cycle+"! This happen when paths can not be paired due to low complexity region... This event will be stored within the missclassified type 2 events file!\nUpper path: "+self.upP.getStrBlocs()+"\nLower path: "+self.lowP.getStrBlocs())
			return
		# We fetch all the blocs with at least one SS within this junction and directly adjacent to the junction
		l=jLowUniq[0].split("-")
		minPos=int(l[0])-1
		maxPos=int(l[1])+1
		for b in self.upP.blocs:
			if b.s<=maxPos and minPos<=b.e: # Bloc within the required range
				self.eUpBlocs.append(b)
		for b in self.lowP.blocs:
			if b.s<=maxPos and minPos<=b.e: # Bloc within the required range
				self.eLowBlocs.append(b)
		if len(self.eLowBlocs)!=2:
			sys.exit("ERROR:\tLower path is not composed by two blocs!\nUpper path: "+self.upP.getStrBlocs()+"\nLower path: "+self.lowP.getStrBlocs())
	
	def setPsi(self, minVar=5):
		# Compute PSI values from the normalised quantifications dictionnary of each path
		# minVar : minimum nomber of normalised reads on a variant (inclusion and exclusion) to compute the psi
		if self.upP==None or self.lowP==None: # One path missing, no computation
			self.psis==[]
			return
		dUp=self.upP.getNormQuantif()
		dLow=self.lowP.getNormQuantif()
		if dUp=={} or dLow=={}: # no quantification
			self.psis==[]
			return
		lCond=sorted(dUp.keys())
		for cond in lCond:
			inc=dUp[cond]
			exc=dLow[cond]
			if inc>=minVar and exc>=minVar:
				psi=str(round(float(inc)/(exc+inc),2))
			else:
				psi="NaN"
			l=cond.split(",")
			self.psis.append("psi"+"+".join(l)+":"+psi)
		
	def getChrStartEndLowerIntron(self):
		if self.state=="regular":
			if self.eventType not in ["insertion","deletion","indel","SNP"]:
				return [self.upP.c, (self.eLowBlocs[0].e+1), (self.eLowBlocs[1].s-1)]
			return [self.eventType]
		return ['multiple']

	def __str__(self):
		r="Event: "+self.eventType+" ; paired? "+str(self.paired)+" ; mapped pairs? "+str(self.mappedPairs)+" ; state: "+self.state
		r+="\nComplexity: "+str(len(self.complex))+" ("+self.complexRegion+"): "
		for bcc,cycle,eventID in self.complex:
			r+="bcc_"+bcc+"|Cycle_"+cycle+" Event n°"+str(eventID)+";"
		r+="\nPSI: "+str(self.psis)
		return r
